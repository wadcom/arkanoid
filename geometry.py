#
# Copyright (c) 2015 Vlad Skvortsov, vss@73rus.com.
#

import collections
import math
import unittest

SIGNIFICANT_DIGITS = 3

class Error(Exception):
    """Base class for all exceptions raised by this module."""


class NegativeVectorLengthError(Error):
    """The vector is of negative length."""


class ZeroVectorLengthError(Error):
    """The vector is of zero length."""


class Coord(object):
    """Coordinates of a point on a two-dimensional plane.

    The coordinates are floating point numbers.
    """
    def __eq__(self, other):
        """Check if the coordinates are almost equal to the other.

        The comparison is done approximately, using SIGNIFICANT digits
        after the decimal point.
        """
        return almost_equal(self.x, other.x) and \
            almost_equal(self.y, other.y)

    def __init__(self, x, y):
        """Initialize the object with coordinates x and y."""
        self.x = x
        self.y = y

    def __repr__(self):
        """Return exact string representation of the coordinates."""
        return 'Coord(x={}, y={})'.format(self.x, self.y)

    def __str__(self):
        """Return an appoximate string representation of the coordinates."""
        fmt = 'Coord(x={{:.{0}f}}, y={{:.{0}f}})'.format(SIGNIFICANT_DIGITS)
        return fmt.format(self.x, self.y)

    def distance(self, from_pt=None):
        """Return the distance from the coordinates to the other point.

        If from_pt is not specified, returns the distance to Coord(0, 0).
        """
        if from_pt is None:
            from_pt = Coord(0, 0)

        return math.sqrt(
            (self.x - from_pt.x) * (self.x - from_pt.x) + \
                (self.y - from_pt.y) * (self.y - from_pt.y))


    @property
    def xy(self):
        return (self.x, self.y)

    @xy.setter
    def xy(self, value):
        self.x, self.y = value

    @property
    def xy_int(self):
        """Return truncated coordinates as a tuple."""
        return (int(self.x), int(self.y))


class _TestCoord(unittest.TestCase):

    def assertDistanceFromOriginIs(self, expected, distance, pt, origin_type):
        self.assertAlmostEqual(expected, distance, places=2,
            msg="Expected distance from {} origin to {} is {}, "
                "not {}".format(origin_type, pt, expected, distance)
        )

    def test_distance(self):
        testcases = [
            (Coord(-1, 2), Coord(-3, 4), 2.83)
        ]

        for pt, from_pt, expected in testcases:
            assert from_pt is not None
            d = pt.distance(from_pt)
            self.assertAlmostEqual(expected, d, places=2,
                msg="Expected distance from {} to {} is {}, not {}".format(
                    from_pt, pt, expected, d)
            )

    def test_distance_from_origin(self):
        testcases = [
            (Coord(4, 0), 4),
            (Coord(4, 0), 4),
            (Coord(6, 0), 6),
            (Coord(7, 0), 7),
            (Coord(8, 9), 12.04),
            (Coord(10, 11), 14.87),
        ]

        for pt, expected in testcases:
            self.assertDistanceFromOriginIs(expected, pt.distance(), pt,
                'implicit')
            self.assertDistanceFromOriginIs(expected,
                pt.distance(Coord(0, 0)), pt, 'explicit')

    def test_equality(self):
        testcases = [
            (Coord(1, 2), Coord(1, 2), True),
            (Coord(1.5, 2), Coord(1, 2), False),
            (Coord(1.0001, 2), Coord(1, 2), True),
        ]

        for c1, c2, expected_equal in testcases:
            self.assertEqual(expected_equal, c1 == c2,
                msg='{!r} is expected to be {}equal to {!r}'.format(
                    c1, '' if expected_equal else 'not ', c2))

    def test_init(self):
        c = Coord(x=1, y=2)
        self.assertEqual(1, c.x)
        self.assertEqual(2, c.y)

    def test_repr(self):
        self.assertEqual('Coord(x=3.0001, y=4.0001)',
            repr(Coord(3.0001, 4.0001)))

    def test_str(self):
        self.assertEqual('Coord(x=3.123, y=4.457)',
            str(Coord(3.1234, 4.4567)))

    def test_xy(self):
        c = Coord(x=2, y=3)
        self.assertEqual((2, 3), c.xy)

    def test_xy_int(self):
        c = Coord(x=2.3, y=3.7)
        self.assertEqual((2, 3), c.xy_int)

    def test_xy_set(self):
        c = Coord(x=2, y=3)
        c.xy = (10, 20)
        self.assertEqual((10, 20), c.xy)


# XXX: should really be in arithmetics.py
def almost_equal(a, b, ndigits=SIGNIFICANT_DIGITS):
    """Determine if floating point numbers a and b are approximately equal.

    Digits after ndigits to the right of the floating point are rounded
    before the comparison.
    """
    if (ndigits > 0) and (ndigits == int(ndigits)):
        a_int = int(a)
        b_int = int(b)
        return (a_int == b_int) and \
            round(abs(a - a_int) * 10**ndigits ) == \
                round(abs(b - b_int) * 10**ndigits)
    else:
        raise ValueError


class _TestAlmostEqual(unittest.TestCase):

    assert SIGNIFICANT_DIGITS == 3

    def test_almost_equal(self):
        testcases = [
            (1, 2, False),
            (1, 1, True),
            (2, 2.001, False),
            (2, 2.00049, True),
            (2, 2.0009, False),
            (2, 2.0005, False),
            (4, -4, False),
            (1, 1.001, False)
        ]

        for a, b, expected_equal in testcases:
            self.assertEqual(
                expected_equal,
                almost_equal(a, b, ndigits=SIGNIFICANT_DIGITS),
                msg='{} is expected to {}be almost equal to {}'.format(
                    a, '' if expected_equal else 'not ', b))

    def test_almost_equal_invalid_ndigits(self):
        for ndigits in [1.1, -1, 0]:
            try:
                almost_equal(1, 1, ndigits=ndigits)
            except ValueError:
                pass
            else:
                self.fail('ValueError not raised for ndigits={}'.format(
                    ndigits))
