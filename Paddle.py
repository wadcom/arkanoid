#
# Copyright (c) 2015 Vlad Skvortsov, vss@73rus.com.
#

import unittest

import pygame

import Edge
import GameObject
import KeyboardState

PADDLE_WIDTH = 100
PADDLE_HEIGHT = 20
PADDLE_STEP = 15
PADDLE_Y = 350


class Paddle(GameObject.GameObject):

    def __init__(self, surface, draw_module=None):
        super(Paddle, self).__init__(surface, draw_module)
        self._pos_x = 0
        self._pos_x_max = surface.get_width() - PADDLE_WIDTH

    def draw(self):
        paddle_color = (128, 128, 128)
        self._draw_module.rect(self._surface, paddle_color,
                               (self.get_pos_x(), PADDLE_Y,
                                PADDLE_WIDTH, PADDLE_HEIGHT)
                               )

    def get_edges(self):
        return Edge.Rect(self._pos_x, PADDLE_Y, PADDLE_WIDTH, PADDLE_HEIGHT)

    def get_pos_x(self):
        return self._pos_x

    def set_pos_x(self, x):
        self._pos_x = x

    def update_state(self, keyboard_state):
        x = self.get_pos_x()

        key_left = (keyboard_state[pygame.K_LEFT] == pygame.KEYDOWN)
        key_right = (keyboard_state[pygame.K_RIGHT] == pygame.KEYDOWN)

        if (key_left and (not key_right)):
            x -= PADDLE_STEP
        if (key_right and (not key_left)):
            x += PADDLE_STEP

        if x < 0:
            x = 0
        if x > self._pos_x_max:
            x = self._pos_x_max

        self.set_pos_x(x)


class TestPaddle(unittest.TestCase):
    # XXX: duplicates the one found in KeyboardState tests

    class Event:

        def __init__(self, action, key):
            self.type = action
            self.key = key

    def create_keyboard_state(self, left, right):
        ks = KeyboardState.KeyboardState()
        if left:
            ks.process_event(self.Event(pygame.KEYDOWN, pygame.K_LEFT))

        if right:
            ks.process_event(self.Event(pygame.KEYDOWN, pygame.K_RIGHT))

        return ks

    def get_width(self):
        return 300

    def rect(self, surface, color, pos_dim):
        self._rect_drawn = True

    def test_draw(self):
        obj = Paddle(surface=self, draw_module=self)
        self._rect_drawn = False
        obj.draw()
        self.assertTrue(self._rect_drawn)

    def test_get_edges(self):
        p = Paddle(surface=self)
        p.set_pos_x(50)
        edges = p.get_edges()
        self.assertEqual(PADDLE_Y, edges.top.coord)
        self.assertEqual(50 + PADDLE_WIDTH - 1, edges.right.coord)
        self.assertEqual(PADDLE_Y + PADDLE_HEIGHT - 1, edges.bottom.coord)
        self.assertEqual(50, edges.left.coord)

    def test_inheritance(self):
        self.assertTrue(issubclass(Paddle, GameObject.GameObject))

    def test_update_state(self):
        p = Paddle(surface=self)
        p.set_pos_x(0)
        p.update_state(self.create_keyboard_state(left=False, right=True))
        self.assertEqual(PADDLE_STEP, p.get_pos_x())

    def test_update_state_double_right(self):
        p = Paddle(surface=self)
        p.set_pos_x(0)
        p.update_state(self.create_keyboard_state(left=False, right=True))
        p.update_state(self.create_keyboard_state(left=False, right=True))
        self.assertEqual(2 * PADDLE_STEP, p.get_pos_x())

    def test_update_state_both_keys(self):
        p = Paddle(surface=self)
        p.set_pos_x(PADDLE_STEP)
        p.update_state(self.create_keyboard_state(left=True, right=True))
        self.assertEqual(PADDLE_STEP, p.get_pos_x())

    def test_update_state_no_keys(self):
        p = Paddle(surface=self)
        p.set_pos_x(PADDLE_STEP)
        ks = KeyboardState.KeyboardState()
        p.update_state(ks)
        self.assertEqual(PADDLE_STEP, p.get_pos_x())

    def test_update_left_edge(self):
        p = Paddle(surface=self)
        p.set_pos_x(0)
        p.update_state(self.create_keyboard_state(left=True, right=False))
        self.assertEqual(0, p.get_pos_x())

    def test_update_right_edge(self):
        p = Paddle(surface=self)
        rightmost_pos = self.get_width() - PADDLE_WIDTH
        p.set_pos_x(rightmost_pos)
        p.update_state(self.create_keyboard_state(left=False, right=True))
        self.assertEqual(rightmost_pos, p.get_pos_x())
