#
# Copyright (c) 2015 Vlad Skvortsov, vss@73rus.com.
#

import unittest

import pygame

class GameObject(object):
  def __init__(self, surface, draw_module=None):
    if draw_module is None:
      draw_module = pygame.draw
    self._draw_module = draw_module
    self._surface = surface

  def draw(self):
    raise NotImplementedError

  def get_draw_module(self):
    return self._draw_module

  def get_surface(self):
    return self._surface

  def update_state(self, keyboard_state):
    raise NotImplementedError

class TestGameObject(unittest.TestCase):
  def test_default_draw_module(self):
    surface = None
    obj = GameObject(surface)
    self.assertEqual(pygame.draw, obj.get_draw_module())

  def test_draw(self):
    window_surface = None
    obj = GameObject(window_surface)
    with self.assertRaises(NotImplementedError):
      obj.draw()

  def test_get_surface(self):
    surface = self
    obj = GameObject(surface)
    self.assertEqual(self, obj.get_surface())

  def test_update_state(self):
    window_surface = None
    obj = GameObject(window_surface)
    keyboard_state = None
    with self.assertRaises(NotImplementedError):
      obj.update_state(keyboard_state)
