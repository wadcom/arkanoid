#! /usr/bin/env python

import pygame

import Ball
import Edge
import KeyboardState
import Paddle

COLOR_WHITE = (255, 255, 255)

# Event loop delay (ms)
EVENT_DELAY = 20

SCREEN_WIDTH = 500
SCREEN_HEIGHT = 400

pygame.init()

window_surface = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT), 0, 32)
pygame.display.set_caption('Hello world!')

ball = Ball.Ball(window_surface, radius=10)
ball.set_velocity(3, 2)

paddle = Paddle.Paddle(window_surface)

keyboard_state = KeyboardState.KeyboardState()

field_edges = Edge.Rect(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)

mid_edge = Edge.Edge(Edge.VERTICAL, SCREEN_WIDTH / 2, SCREEN_HEIGHT / 3,
                     2 * SCREEN_HEIGHT / 3)

quit_requested = False
while not quit_requested:
    # Update state
    paddle.update_state(keyboard_state)

    paddle_edges = paddle.get_edges()

    ball.update_state(keyboard_state, 
        edges=(field_edges.as_list() + [mid_edge] + paddle_edges.as_list())
    )

    # Redraw screen
    window_surface.fill(COLOR_WHITE)
    paddle.draw()
    ball.draw()
    pygame.display.update()

    # Process events
    pygame.time.delay(EVENT_DELAY)
    for event in pygame.event.get():
        keyboard_state.process_event(event)

        if (event.type == pygame.QUIT) or \
                (keyboard_state[pygame.K_ESCAPE] == pygame.KEYDOWN):
            quit_requested = True

pygame.quit()
