#
# Copyright (c) 2015 Vlad Skvortsov, vss@73rus.com.
#

"""This modules provides basic vector arithmetics on a discrete grid."""

import math
import unittest

import geometry


Coord = geometry.Coord


DIR_E = 0
DIR_NE = math.pi / 4
DIR_N = math.pi / 2
DIR_NW = math.pi / 2 + math.pi / 4
DIR_W = math.pi
DIR_SW = math.pi + math.pi / 4
DIR_S = math.pi + math.pi / 2
DIR_SE = 2 * math.pi - math.pi / 4

class Vector(object):
    """An object that represents a vector.

    The vector is defined by its direction (measured in radians, 0 is at
    the "east"), a float length and the coordinates of the vector start.
    """

    def __eq__(self, other):
        return \
            geometry.almost_equal(self.direction, other.direction) and \
            geometry.almost_equal(self.length, other.length) and \
            (self.start == other.start)

    def __init__(self, direction, length, start=Coord(0, 0)):
        if length == 0:
            raise geometry.ZeroVectorLengthError

        if length < 0:
            raise geometry.NegativeVectorLengthError

        self.direction = direction % (2 * math.pi)
        self.length = length
        self.start = start

    def __repr__(self):
        return 'Vector(direction={}, length={}, start={})'.format(
            self.direction, self.length, self.start)

    def decompose(self, basis):
        """Decompose a vector to a sum of vectors given a basis ray.

        Basis is a direction (in radians). Returns a list of one or two
        vectors.

        The first returned vector is always collinear to the basis
        ray (but its direction could be opposite).

        The second returned vector is always perpendicular to the basis.

        The sum of vectors in the list returned is equal to self (the
        vector this method is called on).
        """
        result = []

        try:
            result.append(self.projection(basis))
        except geometry.ZeroVectorLengthError:
            pass

        try:
            result.append(self.projection(basis + math.pi/2))
        except geometry.ZeroVectorLengthError:
            pass

        return result

    @property
    def end(self):
        """Return coordinates of the vector end."""
        x = self.length * math.cos(self.direction)
        y = self.length * math.sin(self.direction)
        return Coord(x + self.start.x, y + self.start.y)

    @end.setter
    def end(self, pos):
        """Assigning vector end coordinates is not supported.

        This will raise NotImplementedError.
        """
        raise NotImplementedError

    @classmethod
    def from_coord(cls, start, end):
        """Create a vector given coordinates of its start and end.

        Raises geometry.ZeroVectorLengthError if start and and are the
        same.
        """
        length = start.distance(end)
        if length == 0:
            raise geometry.ZeroVectorLengthError
        direction = math.acos((end.x - start.x) / length)

        if end.y < start.y:
            direction = 2 * math.pi - direction

        return cls(direction=direction, length=length, start=start)

    def projection(self, basis):
        """Return a projection of the vector onto a basis ray.

        The return value is vector that can be oriented either along
        or opposite to the basis. The start of the projection vector
        is the same as the start of the original vector.

        If the vector is perpendicular to the basis,
        geometry.ZeroVectorLengthError is raised.
        """
        direction = basis
        length = self.length * math.cos(self.direction - basis)
        if length < 0:
            direction += math.pi
            length *= -1

        if geometry.almost_equal(length, 0):
            raise geometry.ZeroVectorLengthError

        return Vector(direction, length, start=self.start)


class _TestVector(unittest.TestCase):

    def test_equality(self):
        testcases = [
            (
                Vector(direction=DIR_SE, length=15),
                Vector(direction=DIR_SE, length=15),
                True
            ),
            (
                Vector(direction=DIR_SE, length=15),
                Vector(direction=DIR_NE, length=15),
                False
            ),
            (
                Vector(direction=DIR_SE, length=14),
                Vector(direction=DIR_SE, length=15),
                False
            ),
            (
                Vector(direction=DIR_NE, length=20, start=Coord(1, 2)),
                Vector(direction=DIR_NE, length=20, start=Coord(2, 3)),
                False
            ),
            (
                Vector(direction=1, length=14),
                Vector(direction=1.001, length=14),
                False
            ),
            (
                Vector(direction=2, length=14),
                Vector(direction=2.0009, length=14),
                False
            ),
            (
                Vector(direction=DIR_SE, length=14),
                Vector(direction=DIR_SE, length=14.0004),
                True
            ),
            (
                Vector(direction=DIR_SE, length=14),
                Vector(direction=DIR_SE, length=14.001),
                False
            ),
        ]
        for v1, v2, should_eq in testcases:
            self.assertEqual(
                should_eq, v1 == v2,
                msg='{} and {} are{}expected to be equal'.format(
                    v1, v2, ' ' if should_eq else ' not '
                ))

    def test_creation(self):
        v = Vector(direction=(math.pi / 2), length=10)
        self.assertEqual(math.pi / 2, v.direction)
        self.assertEqual(10, v.length)

    def test_creation_from_coord(self):
        testcases = [
            (Coord(2, 3), Coord(1, 4),
                Vector(DIR_NW, 1.414, start=Coord(2, 3))),

            (Coord(250, 200), Coord(250, 110),
                Vector(DIR_S, 90, start=Coord(250, 200))),

            (Coord(250, 200), Coord(162, 181),
                Vector(3.354, 90.028, start=Coord(250, 200)))
        ]

        for start, end, expected in testcases:
            result = Vector.from_coord(start, end)
            self.assertEqual(expected, result,
                msg='Vector {} -> {} expected to be {}, not {}'.format(
                    start, end, expected, result))


    def test_creation_from_coord_point(self):
        with self.assertRaises(geometry.ZeroVectorLengthError):
            Vector.from_coord(Coord(2, 3), Coord(2, 3))

    def test_decompose(self):
        v = Vector(DIR_NW, 40)
        self.assertEqual([Vector(DIR_NW, 40)], v.decompose(DIR_NW))

    def test_decompose_2(self):
        v = Vector(DIR_NW, 40)
        self.assertEqual([Vector(DIR_W, 28.284), Vector(DIR_N, 28.284)],
            v.decompose(DIR_E))

    def test_direction_normalization(self):
        testcases = [
            (2 * math.pi, 0),
            (2 * math.pi + math.pi / 4, math.pi / 4),
            (- math.pi / 4, 2 * math.pi - math.pi / 4)
        ]

        for direction, expected in testcases:
            v = Vector(direction=direction, length=2)
            self.assertEqual(
                expected, v.direction,
                msg="{} normalized to {}, expected {}".format(
                    direction, v.direction, expected))

    def test_end(self):
        v = Vector(direction=DIR_N, length=10)
        self.assertEqual(Coord(0, 10), v.end)

    def test_end_2(self):
        v = Vector(direction=DIR_W, length=10)
        self.assertEqual(Coord(-10, 0), v.end)

    def test_end_with_start(self):
        v = Vector(direction=DIR_E, length=10, start=Coord(12, 34))
        self.assertEqual(Coord(22, 34), v.end)

    def test_end_set(self):
        v = Vector(direction=(-math.pi), length=10)
        with self.assertRaises(NotImplementedError):
            v.end = Coord(5, 5)

    def test_length_negative(self):
        with self.assertRaises(geometry.NegativeVectorLengthError):
            Vector(direction=1, length=-10)

    def test_length_zero(self):
        with self.assertRaises(geometry.ZeroVectorLengthError):
            Vector(direction=1, length=0)

    def test_projection(self):
        testcases = [
            (Vector(DIR_SW, 20), DIR_W, Vector(DIR_W, 14.142)),
            (Vector(DIR_SW, 20), DIR_E, Vector(DIR_W, 14.142)),
            (Vector(DIR_SW, 20), DIR_S, Vector(DIR_S, 14.142)),
            (Vector(DIR_SW, 20, start=Coord(2, 4)), DIR_S,
                Vector(DIR_S, 14.142, start=Coord(2, 4))),
        ]

        for original, basis, expected in testcases:
            self.assertEquals(expected, original.projection(basis))

    def test_projection_perpendicular(self):
        v = Vector(DIR_N, 1)
        with self.assertRaises(geometry.ZeroVectorLengthError):
            print v.projection(DIR_E)

    def test_start(self):
        v = Vector(direction=DIR_S, length=10, start=Coord(123, 456))
        self.assertEqual(Coord(123, 456), v.start)

    def test_start_change(self):
        v = Vector(direction=DIR_W, length=20)
        v.start = Coord(-5, 20)
        self.assertEqual(Coord(-25, 20), v.end)

    def test_start_default(self):
        v = Vector(direction=DIR_S, length=10)
        self.assertEqual(Coord(0, 0), v.start)
