#
# Copyright (c) 2015 Vlad Skvortsov, vss@73rus.com.
#

import math
import unittest

import Edge
import GameObject

class Ball(GameObject.GameObject):

    def __init__(self, surface, radius, draw_module=None, pos=(10, 10)):
        super(Ball, self).__init__(surface, draw_module)
        self._pos_x, self._pos_y = pos
        self._radius = radius
        self._v_x = self._v_y = 0

    def _compute_step(self):
        m = float(max(abs(self._v_x), abs(self._v_y)))
        return (self._v_x / m, self._v_y / m)

    # XXX: seems weird. A Ball method, returning another Ball?
    def _move_a_step(self):
        v_x, v_y = self._compute_step()
        return Ball(surface=self._surface, radius=self._radius,
            pos=(self._pos_x + v_x, self._pos_y + v_y)
        )

    def bounce(self, horizontally, vertically):
        if horizontally:
            self._v_y *= -1

        if vertically:
            self._v_x *= -1

    def collides_with(self, edge):
        if edge.orientation == Edge.HORIZONTAL:
            pos_primary = self._pos_y
            pos_secondary = self._pos_x
        else:
            assert edge.orientation == Edge.VERTICAL
            pos_primary = self._pos_x
            pos_secondary = self._pos_y

        if (pos_secondary >= edge.low) and (pos_secondary <= edge.high):
            return \
                (pos_primary >= (edge.coord - self._radius)) \
                and (pos_primary <= (edge.coord + self._radius))
        else:
            dist_s_low = pos_secondary - edge.low
            dist_s_high = pos_secondary - edge.high
            dist_p = pos_primary - edge.coord

            dist_low = dist_p * dist_p + dist_s_low * dist_s_low
            dist_high = dist_p * dist_p + dist_s_high * dist_s_high

            r_square = self._radius * self._radius

            return (dist_low <= r_square) or (dist_high <= r_square)

    def draw(self):
        self._draw_module.circle(
          self._surface, (1, 2, 3), (int(self._pos_x), int(self._pos_y)),
          self._radius, 0
        )

    def get_pos(self):
        return (self._pos_x, self._pos_y)

    def get_radius(self):
        return self._radius

    def get_velocity(self):
        return (self._v_x, self._v_y)

    def set_pos(self, x, y):
        self._pos_x = x
        self._pos_y = y

    def set_velocity(self, v_x, v_y):
        self._v_x = v_x
        self._v_y = v_y

    def update_state(self, keyboard_state, edges):
        self._pos_x += self._v_x
        self._pos_y += self._v_y

        bounce_hor = bounce_vert = False
        for edge in edges:
            if self.collides_with(edge):
                if edge.orientation == Edge.HORIZONTAL:
                    bounce_hor = True
                else:
                    assert edge.orientation == Edge.VERTICAL
                    bounce_vert = True

        self.bounce(bounce_hor, bounce_vert)


class TestBall(unittest.TestCase):

    def circle(self, surface, color, pos, radius, width):
        self._circle_drawn = True
        self._circle_pos = pos

    def test_bounce(self):
        testcases = [
            ((10, 20), False, False, (10, 20)),
            ((5, 6), False, True, (-5, 6)),
            ((7, -8), True, False, (7, 8)),
            ((-1, 8), True, True, (1, -8))
        ]

        for (init_vx, init_vy), hor, vert, expected in testcases:
            b = Ball(surface=None, radius=5)
            b.set_velocity(init_vx, init_vy)
            b.bounce(hor, vert)
            self.assertEqual(expected, b.get_velocity())

    def test_collides_with(self):
        hor = Edge.HORIZONTAL
        vert = Edge.VERTICAL
        testcases = [
            ###
            ### Horizontal edge
            ###

            # Way off to the left
            ('way off left, above', hor, (50, 80), False),
            ('way off left, touching above', hor, (40, 95), False),
            ('way off left, touching below', hor, (70, 105), False),
            ('way off left, below', hor, (80, 125), False),

            # Close to the left end
            ('close on left, above', hor, (195, 80), False),
            ('close on left, touching above', hor, (195, 95), True),
            ('close on left, touching below', hor, (195, 105), True),
            ('close on left, below', hor, (195, 125), False),

            # Middle
            ('middle, above', hor, (250, 80), False),
            ('middle, touching above', hor, (240, 95), True),
            ('middle, touching below', hor, (270, 105), True),
            ('middle, below', hor, (290, 125), False),

            # Close to the right end
            ('close on right, above', hor, (305, 80), False),
            ('close on right, touching above', hor, (305, 95), True),
            ('close on right, touching below', hor, (305, 105), True),
            ('close on right, below', hor, (305, 125), False),

            # Way off to the right
            ('way off right, above', hor, (330, 70), False),
            ('way off right, touching above', hor, (340, 99), False),
            ('way off right, touching below', hor, (320, 109), False),
            ('way off right, below', hor, (315, 140), False),

            ###
            ### Vertical edge
            ###

            # Way off to the top
            ('way off top, on the left', vert, (80, 100), False),
            ('way off top, touching on the left', vert, (95, 150), False),
            ('way off top, touching on the right', vert, (105, 180), False),
            ('way off top, on the right', vert, (220, 50), False),

            # Close to the top end
            ('close to the top, on the left', vert, (80, 195), False),
            ('close to the top, touching on the left', vert, (95, 195), True),
            ('close to the top, touching on the right', vert, (105, 195), 
                True),
            ('close to the top, on the right', vert, (120, 195), False),

            # Middle
            ('middle, on the left', vert, (80, 260), False),
            ('middle, touching on the left', vert, (92, 270), True),
            ('middle, touching on the right', vert, (104, 220), True),
            ('middle, on the right', vert, (111, 240), False),

            # Close to the bottom end
            ('close to the bottom, on the left', vert, (85, 303), False),
            ('close to the bottom, touching on the left', vert, (97, 305),
                True),
            ('close to the bottom, touching on the right', vert, (105, 307),
                True),
            ('close to the bottom, on the right', vert, (125, 306), False),

            # Way below
            ('way below, on the left', vert, (75, 400), False),
            ('way below, touching on the left', vert, (95, 330), False),
            ('way below, touching on the right', vert, (108, 350), False),
            ('way below, on the right', vert, (150, 360), False)
        ]

        for (descr, orientation, (x, y), expected) in testcases:
            b = Ball(surface=None, radius=10)
            e = Edge.Edge(orientation, 100, 200, 300)
            b.set_pos(x, y)
            self.assertEqual(expected, b.collides_with(e),
                msg='{} ({} edge): expected {}'.format(
                    descr,
                    { hor: 'horizontal', vert: 'vertical'}[orientation],
                    expected
                )
            )

    def test_draw(self):
        window_surface = None
        obj = Ball(window_surface, radius=None, draw_module=self)
        self._circle_drawn = False
        obj.draw()
        self.assertTrue(self._circle_drawn)

    def test_draw_fractional_coordinates(self):
        b = Ball(surface=None, radius=30, draw_module=self, pos=(1.2, 3.9))
        self._circle_pos = None
        b.draw()
        self.assertEqual((1, 3), self._circle_pos)

    def test_inheritance(self):
        self.assertTrue(issubclass(Ball, GameObject.GameObject))

    def test_init_pos(self):
        b = Ball(surface=None, radius=None, pos=(12, 34))
        self.assertEqual((12, 34), b.get_pos())

    def test_radius(self):
        b = Ball(surface=None, radius=23)
        self.assertEqual(23, b.get_radius())

    def test_update_state_bounce(self):
        b = Ball(surface=None, radius=30)
        b.set_pos(100, 200)
        v_x, v_y = 10, 20
        b.set_velocity(v_x, v_y)
        e = Edge.Edge(Edge.HORIZONTAL, 210, 50, 150)
        b.update_state(keyboard_state=None, edges=[e])
        self.assertNotEqual((v_x, v_y), b.get_velocity())

    def test_update_state_velocity(self):
        b = Ball(surface=None, radius=20)
        b.set_pos(100, 100)
        b.set_velocity(2, 3)
        b.update_state(keyboard_state=None, edges=[])
        self.assertEqual((102, 103), b.get_pos())

class TestBallDynamics(unittest.TestCase):

    def test_compute_step(self):
        b = Ball(surface=None, radius=5)
        b.set_velocity(3, 5)
        step_x, step_y = b._compute_step()
        self.assertAlmostEqual(0.6, step_x)
        self.assertEqual(1, step_y)

    def test_compute_step_negative(self):
        b = Ball(surface=None, radius=5)
        b.set_velocity(-10, 0)
        step_x, step_y = b._compute_step()
        self.assertAlmostEqual(-1, step_x)
        self.assertEqual(0, step_y)

    def test_move_a_step(self):
        testcases = [
            ((5, 0), (101, 200)),
            ((7, 0), (101, 200)),
            ((0, 4), (100, 201)),
            ((0, -8), (100, 199)),
            ((10, 20), (100.5, 201)),
            ((-40, 10), (99, 200.25))
        ]
        for (v_x, v_y), (exp_x, exp_y) in testcases:
            b = Ball(surface=None, radius=10)
            b.set_pos(100, 200)
            b.set_velocity(v_x, v_y)
            new_ball = b._move_a_step()
            x, y = new_ball.get_pos()
            self.assertAlmostEqual(exp_x, x)
            self.assertAlmostEqual(exp_y, y)

    # XXX: an acceptance test, really
    # XXX: add more tests like this
    def _XXX_test_staying_in_collision(self):
        b = Ball(surface=None, radius=20)
        b.set_pos(100, 200)
        e = Edge.Edge(Edge.VERTICAL, 101, 220, 300)
        assert not b.collides_with(e)

        b.set_velocity(1, 10)
        b.update_state(keyboard_state=None, edges=[e])

        self.assertFalse(b.collides_with(e))


