#! /usr/bin/env python

import collections
import math
import unittest

import pygame

import geometry
import KeyboardState
import Vector

Coord = geometry.Coord

COLOR_BALL = (0, 200, 0)
COLOR_BALL_CENTER = (0, 0, 0)
COLOR_BOUNCE_DECOMPOSED = (128, 128, 0)
COLOR_BOUNCE = (255, 0, 0)
COLOR_CONTACT_POINT = (128, 0, 0)
COLOR_SCREEN = (255, 255, 255)
COLOR_VELOCITY = (0, 0, 128)
COLOR_VELOCITY_DECOMPOSED = (0, 200, 200)

SCREEN_WIDTH = 500
SCREEN_HEIGHT = 400

CONTACT_POINT_RADIUS = 3

# Event loop delay (ms)
EVENT_DELAY = 20

ARROW_LENGTH = 7

def arrow_ends(start, end, arrow_length=ARROW_LENGTH):
    ARROW_ANGLE = math.pi / 12

    v_angle = Vector.Vector.from_coord(start, end).direction 
    left_angle = v_angle + math.pi - ARROW_ANGLE
    left_pos = Coord(
        x=int(arrow_length * math.cos(left_angle) + end.x),
        y=int(arrow_length * math.sin(left_angle) + end.y)
    )

    right_angle = v_angle + math.pi + ARROW_ANGLE
    right_pos = Coord(
        x=int(arrow_length * math.cos(right_angle) + end.x),
        y=int(arrow_length * math.sin(right_angle) + end.y)
    )

    return (left_pos, right_pos)


class TestArrowEnds(unittest.TestCase):

    def test_end(self):
        testcases = [
            (Coord(0, 0), Coord(100, 0), (Coord(95, 1), Coord(95, -1))),
            (Coord(0, 0), Coord(0, 100), (Coord(-1, 95), Coord(1, 95))),
            (Coord(250, 200), Coord(250, 110),
                (Coord(251, 114), Coord(248, 114)))
        ]

        for start, end, expected in testcases:
            result = arrow_ends(start, end, arrow_length=5)
            self.assertEqual(expected, result,
                msg='Arrow ends for vector {} -> {} are expected to be\n'
                    '{} but are\n'
                    '{}'.format(start, end, expected, result))

def draw_arrow(surface, color, start, end):
    pygame.draw.line(surface, color, start.xy, end.xy)

    left_end, right_end = arrow_ends(start, end)

    pygame.draw.line(surface, color, end.xy, left_end.xy)
    pygame.draw.line(surface, color, end.xy, right_end.xy)


def main():
    pygame.init()

    window_surface = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT), 0, 32)
    pygame.display.set_caption('Visualizer')

    keyboard_state = KeyboardState.KeyboardState()

    ball_pos = Coord(x=(SCREEN_WIDTH / 2), y=(SCREEN_HEIGHT / 2))
    ball_radius = 50

    contact_point = Vector.Vector(direction=Vector.DIR_N + 1, length=ball_radius,
        start=ball_pos)

    velocity = Vector.Vector(direction=Vector.DIR_NW, length=90,
        start=ball_pos)

    quit_requested = False
    while not quit_requested:

        # XXX: the step should be a constant
        if keyboard_state[pygame.K_UP] == pygame.KEYDOWN:
            velocity.length += 1

        if keyboard_state[pygame.K_DOWN] == pygame.KEYDOWN:
            if velocity.length > 1:
                velocity.length -= 1

        if keyboard_state[pygame.K_LEFT] == pygame.KEYDOWN:
            velocity.direction += 0.01

        if keyboard_state[pygame.K_RIGHT] == pygame.KEYDOWN:
            velocity.direction -= 0.01

        if keyboard_state[pygame.K_z] == pygame.KEYDOWN:
            contact_point.direction += 0.01

        if keyboard_state[pygame.K_x] == pygame.KEYDOWN:
            contact_point.direction -= 0.01

        # Redraw screen
        window_surface.fill(COLOR_SCREEN)

        pygame.draw.circle(window_surface, COLOR_BALL, ball_pos.xy,
            ball_radius, 1)
        pygame.draw.circle(window_surface, COLOR_BALL_CENTER, ball_pos.xy, 1,
            0)

        pygame.draw.circle(window_surface, COLOR_CONTACT_POINT,
            contact_point.end.xy_int, CONTACT_POINT_RADIUS, 0)

        draw_arrow(window_surface, COLOR_VELOCITY, ball_pos, velocity.end)

        velocity_towards_contact_point = \
            Vector.Vector(direction=contact_point.direction,
                length=velocity.projection(contact_point.direction).length,
                start=ball_pos)

        draw_arrow(window_surface, COLOR_VELOCITY_DECOMPOSED, ball_pos,
            velocity_towards_contact_point.end)

        draw_arrow(window_surface, COLOR_VELOCITY_DECOMPOSED,
            velocity_towards_contact_point.end, velocity.end)

        bounce_decomposed = Vector.Vector.from_coord(
            velocity_towards_contact_point.end, velocity.end)
        bounce_decomposed.start = ball_pos

        draw_arrow(window_surface, COLOR_BOUNCE_DECOMPOSED,
            bounce_decomposed.start, bounce_decomposed.end)

        bounce_reversed = Vector.Vector.from_coord(
            velocity_towards_contact_point.end,
            velocity_towards_contact_point.start)
        bounce_reversed.start = ball_pos

        draw_arrow(window_surface, COLOR_BOUNCE_DECOMPOSED,
            bounce_reversed.start, bounce_reversed.end)

        bounce_end = Coord(
            ball_pos.x + \
                bounce_decomposed.end.x - bounce_decomposed.start.x + \
                bounce_reversed.end.x - bounce_reversed.start.x,
            ball_pos.y + \
                bounce_decomposed.end.y - bounce_decomposed.start.y + \
                bounce_reversed.end.y - bounce_reversed.start.y)

        draw_arrow(window_surface, COLOR_BOUNCE, ball_pos, bounce_end)

        pygame.display.update()

        # Process events
        pygame.time.delay(EVENT_DELAY)
        for event in pygame.event.get():
            keyboard_state.process_event(event)

            if (event.type == pygame.QUIT) or \
                    (keyboard_state[pygame.K_ESCAPE] == pygame.KEYDOWN):
                quit_requested = True

    pygame.quit()

if __name__ == '__main__':
    main()
