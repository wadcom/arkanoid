#
# Copyright (c) 2015 Vlad Skvortsov, vss@73rus.com.
#

import unittest

import pygame

class KeyboardState(object):
  def __getitem__(self, key):
    try:
      return self._keys[key]
    except KeyError:
      return pygame.KEYUP

  def __init__(self):
    self._keys = {}

  def process_event(self, event):
    if event.type in (pygame.KEYUP, pygame.KEYDOWN):
      self._keys[event.key] = event.type

class TestKeyboardState(unittest.TestCase):
  class Event:
    def __init__(self, action, key):
      self.type = action
      self.key = key

  def test_clean_start(self):
    ks = KeyboardState()
    self.assertEqual(pygame.KEYUP, ks[pygame.K_ESCAPE])

  def test_double_down(self):
    ks = KeyboardState()
    ks.process_event(self.Event(pygame.KEYDOWN, pygame.K_LEFT))
    ks.process_event(self.Event(pygame.KEYDOWN, pygame.K_RIGHT))
    self.assertEqual(pygame.KEYDOWN, ks[pygame.K_LEFT])

  def test_down(self):
    ks = KeyboardState()
    ks.process_event(self.Event(pygame.KEYDOWN, pygame.K_LEFT))
    self.assertEqual(pygame.KEYDOWN, ks[pygame.K_LEFT])

  def test_interspersed_down_up(self):
    ks = KeyboardState()
    ks.process_event(self.Event(pygame.KEYDOWN, pygame.K_LEFT))
    ks.process_event(self.Event(pygame.KEYDOWN, pygame.K_RIGHT))
    ks.process_event(self.Event(pygame.KEYUP, pygame.K_LEFT))
    self.assertEqual(pygame.KEYDOWN, ks[pygame.K_RIGHT])

  def test_single_up(self):
    ks = KeyboardState()
    ks.process_event(self.Event(pygame.KEYUP, pygame.K_LEFT))
    self.assertEqual(pygame.KEYUP, ks[pygame.K_LEFT])
