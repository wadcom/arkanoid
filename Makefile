all:

TEST_MODULES = \
	Ball \
	Edge \
	geometry \
	KeyboardState \
	GameObject \
	Paddle \
	Vector \
	visualize

check:
	python -m unittest $(TEST_MODULES)
