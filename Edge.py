#
# Copyright (c) 2015 Vlad Skvortsov, vss@73rus.com.
#

import unittest

HORIZONTAL = 0
VERTICAL = 1

# XXX: add Horizontal and Vertical classes?

class Edge(object):

    def __eq__(self, other):
        return (self.coord == other.coord) and (self.low == other.low) and \
            (self.high == other.high) and \
            (self.orientation == other.orientation)

    def __init__(self, orientation, coord, low, high):
        if low > high:
            raise ValueError
        self.coord = coord
        self.low = low
        self.high = high
        self.orientation = orientation

    def __repr__(self):
        return 'Edge(orientation={}, coord={}, low={}, high={})'.format(
                'HORIZONTAL' if self.orientation == HORIZONTAL \
                    else 'VERTICAL',
                self.coord, self.low, self.high
            )

class TestEdge(unittest.TestCase):

    def test_equality(self):
        e1 = Edge(HORIZONTAL, 1, 2, 3)
        e2 = Edge(HORIZONTAL, 1, 2, 3)
        self.assertTrue(e1 == e2)

    def test_low_high(self):
        with self.assertRaises(ValueError):
            Edge(HORIZONTAL, 10, 300, 200)

    def test_create_horizontal(self):
        e = Edge(HORIZONTAL, 10, 200, 300)
        self.assertEqual(HORIZONTAL, e.orientation)
        self.assertEqual(10, e.coord)
        self.assertEqual(200, e.low)
        self.assertEqual(300, e.high)

    def test_create_vertical(self):
        e = Edge(VERTICAL, 10, 200, 300)
        self.assertEqual(10, e.coord)
        self.assertEqual(VERTICAL, e.orientation)
        self.assertEqual(200, e.low)
        self.assertEqual(300, e.high)

    def test_repr(self):
        e = Edge(HORIZONTAL, 1, 2, 3)
        self.assertEqual(
            'Edge(orientation=HORIZONTAL, coord=1, low=2, high=3)',
            repr(e)
        )


class Rect(object):

    def __init__(self, x, y, w, h):
        self.top = Edge(HORIZONTAL, y, x, x + w - 1)
        self.right = Edge(VERTICAL, x + w - 1, y, y + h - 1)
        self.bottom = Edge(HORIZONTAL, y + h - 1, x, x + w - 1)
        self.left = Edge(VERTICAL, x, y, y + h - 1)

    def as_list(self):
        return [self.top, self.right, self.bottom, self.left]

class TestRect(unittest.TestCase):

    def test_as_list(self):
        r = Rect(1, 2, 3, 4)
        self.assertEqual([r.top, r.right, r.bottom, r.left], r.as_list())

    def test_init(self):
        r = Rect(10, 25, 70, 90)
        top = Edge(HORIZONTAL, 25, 10, 79)
        right = Edge(VERTICAL, 79, 25, 114)
        bottom = Edge(HORIZONTAL, 114, 10, 79)
        left = Edge(VERTICAL, 10, 25, 114)
        self.assertEqual(top, r.top)
        self.assertEqual(right, r.right)
        self.assertEqual(bottom, r.bottom)
        self.assertEqual(left, r.left)
